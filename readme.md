#Descripcion de la app para el curso apache cordova desarrollado por Ruben Hector Mendez mendez "zendAR"

#Para preparar la app corremos desde la console ubicados en el proyecto :
npm run preparar
#Para correr la app desde consola corremos los comandos:
# para correrlo en la platforms Android:::
npm run android
#para correrlo en la plataforma browser
npm run browser

***Pretende ser una pequeña app que sea capaz de capturar audio, imagen y video desde el dispositivo movil en este caso para la plataforma android.
***La misma pretende Lograr almacenar y recuperarlos desde el propio dispositivo y poder enviarlos a redes sociales.
***Por cuestiones de tiempo se implementara solo la captura de imagenes nada mas.
